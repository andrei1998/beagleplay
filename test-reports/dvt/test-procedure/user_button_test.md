Power Button Short press test
=============================

install evtest

```
sudo apt-get update && sudo apt-get install evtest
```

run gpio USR button test.

```
debian@BeagleBone:~$ evtest
No device specified, trying to scan all of /dev/input/event*
Not running as root, no devices may be available.
Available devices:
/dev/input/event0:	tps65219-pwrbutton
/dev/input/event1:	gpio-keys
Select the device event number [0-1]: 1
Input driver version is 1.0.1
Input device ID: bus 0x19 vendor 0x1 product 0x1 version 0x100
Input device name: "gpio-keys"
Supported events:
  Event type 0 (EV_SYN)
  Event type 1 (EV_KEY)
    Event code 256 (BTN_0)
Key repeat handling:
  Repeat type 20 (EV_REP)
    Repeat code 0 (REP_DELAY)
      Value    250
    Repeat code 1 (REP_PERIOD)
      Value     33
Properties:
Testing ... (interrupt to exit)
Event: time 1665553414.754533, type 1 (EV_KEY), code 256 (BTN_0), value 1
Event: time 1665553414.754533, -------------- SYN_REPORT ------------
Event: time 1665553414.990591, type 1 (EV_KEY), code 256 (BTN_0), value 0
Event: time 1665553414.990591, -------------- SYN_REPORT ------------
Event: time 1665553415.762522, type 1 (EV_KEY), code 256 (BTN_0), value 1
Event: time 1665553415.762522, -------------- SYN_REPORT ------------
Event: time 1665553415.998512, type 1 (EV_KEY), code 256 (BTN_0), value 0
Event: time 1665553415.998512, -------------- SYN_REPORT ------------
Event: time 1665553416.570518, type 1 (EV_KEY), code 256 (BTN_0), value 1
Event: time 1665553416.570518, -------------- SYN_REPORT ------------
Event: time 1665553416.758545, type 1 (EV_KEY), code 256 (BTN_0), value 0
Event: time 1665553416.758545, -------------- SYN_REPORT ------------
Event: time 1665553417.278483, type 1 (EV_KEY), code 256 (BTN_0), value 1
Event: time 1665553417.278483, -------------- SYN_REPORT ------------
Event: time 1665553417.450510, type 1 (EV_KEY), code 256 (BTN_0), value 0
Event: time 1665553417.450510, -------------- SYN_REPORT ------------
Event: time 1665553417.962518, type 1 (EV_KEY), code 256 (BTN_0), value 1
Event: time 1665553417.962518, -------------- SYN_REPORT ------------
Event: time 1665553418.150444, type 1 (EV_KEY), code 256 (BTN_0), value 0
Event: time 1665553418.150444, -------------- SYN_REPORT ------------
Event: time 1665553418.730525, type 1 (EV_KEY), code 256 (BTN_0), value 1
Event: time 1665553418.730525, -------------- SYN_REPORT ------------
Event: time 1665553418.890521, type 1 (EV_KEY), code 256 (BTN_0), value 0
Event: time 1665553418.890521, -------------- SYN_REPORT ------------

```
