# BeagleBoard.org BeaglePlay
_Easy. Affordable. Connected. Open._

[BeaglePlay](https://beagleplay.org) is Linux computing made simple and fun
with 1,000s of available off-the-shelf sensors, actuators, indicators and
connectivity options over mikroBUS, Grove, and QWIIC connections, a TI Sitara
AM625 system-on-chip with 1.4GHz quad-core Arm Cortex-A53, PRU and M4
microcontrollers, Gigabit Ethernet, full-size HDMI, USB, 5GHz, 2.4GHz and
sub-1GHz wireless, and single-pair Ethernet with power-over-data-line. Camera
and touchscreen display ribbon-cable connectors also included.

No breadboarding expertise needed to expand this system, just connect, power,
and leverage the massive ecosystem of Linux drivers. Want a remote sensor?
Utilize BeagleConnect® technology over the sub-1GHz IEEE 802.15.4 wireless
network to a BeagleConnect® Freedom up to 1km away.

## Certified open source

[![](OSHW_mark_US002174.png)](https://certification.oshwa.org/us002174.html)

## Board image

[![](https://www.beagleboard.org/app/uploads/2023/03/font.png-1.webp)](https://beagleplay.org)

## Block diagram

[![](Design/System_Block_Diagram.png)](https://docs.beagleboard.org/latest/boards/beagleplay)

## Links

* Terms and conditions: https://docs.beagleboard.org/latest/boards/terms-and-conditions
* Documentation: https://docs.beagleboard.org/latest/boards/beagleplay
* Design: https://git.beagleboard.org/beagleplay/beagleplay
* Support: https://forum.beagleboard.org/tag/play
* Repair: https://www.beagleboard.org/rma
* Purchase: https://beagleplay.org

