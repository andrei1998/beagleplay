List of testing instructions
============================

* [ADC](adc_testing.md)
* [LEDs](led_usr0-4.md)
* [Power button short press](power_button_short_press_test.md)
* [USR button](user_button_test.md)
* [Wlan basic scan](wlan_basic_scan_test.md)
* [RTC testing](rtc_testing.md)
* [Testing 25MHz SoC crystal](test_25_mhz_time.md)
* [Testing 32k SoC crystal with RTC](test_32k_rtc.md)
* [Testing HDMI](test_hdmi.md)
* [Testing Power Button](powerbutton_testing.md)
* [Testing OLDI](test_oldi.md)
* [Testing Camera](test_camera.md)
* [Testing SPE](test_spe.md)
* [Testing USB Type C](test_usb_typec.md)
* [CC1352P7 Flashing with JTAG](cc1352p7-openocd-jtag-flashing.md)
* [CC1352P7 Testing SubGHz and 2.4GHz ](test_cc1352.md)
