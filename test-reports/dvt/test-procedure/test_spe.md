Testing Single Pair Ethernet
============================

Prereqs
-------

* Ensure  board is disconnected from internet and local network (after required packages are installed).
* [RJ11 cable](https://www.amazon.com/dp/B086YLZLQ9)
* You will need two boards to do back to back testing

Board setup:
-----------
![Board1](img/spe_board1.jpg?raw=true "board1 connection")
![Board2](img/spe_board2.jpg?raw=true "board2 connection")
![LT panel 1](img/spe_setup.jpg?raw=true "SPE back to back connection")

Packages Setup:
---------------

```
sudo apt-get install iperf3
```

Board configuration for testing
-------------------------------

*If we disconnect from wlan and other local networks, you maynot have network
address space conflict for this testing*

board1:

```
sudo ifconfig eth1 192.168.10.1/24 
```

board2:

```
sudo ifconfig eth1 192.168.10.2/24 
```


Basic Testing:
--------------

from board1:

```
ping 192.168.10.2
```

from board2:

```
ping 192.168.10.1
```


Ctrl+c to stop

Logs:

```
$ ping 192.168.10.1
PING 192.168.10.1 (192.168.10.1) 56(84) bytes of data.
64 bytes from 192.168.10.1: icmp_seq=1 ttl=64 time=0.822 ms
64 bytes from 192.168.10.1: icmp_seq=2 ttl=64 time=0.945 ms
64 bytes from 192.168.10.1: icmp_seq=3 ttl=64 time=1.11 ms
64 bytes from 192.168.10.1: icmp_seq=4 ttl=64 time=1.04 ms
64 bytes from 192.168.10.1: icmp_seq=5 ttl=64 time=1.01 ms
```

Performance testing/Long duration testing:
------------------------------------------

board1:

```
 iperf3 -s -p 7575
```

board2:

```
 iperf3 -c 192.168.10.1 -p 7575
```


Logs:

board1:

```
debian@BeagleBone:~$ iperf3 -s -p 7575
-----------------------------------------------------------
Server listening on 7575
-----------------------------------------------------------
Accepted connection from 192.168.10.2, port 57000
[  5] local 192.168.10.1 port 7575 connected to 192.168.10.2 port 57014
[ ID] Interval           Transfer     Bitrate
[  5]   0.00-1.00   sec  1.09 MBytes  9.10 Mbits/sec                  
[  5]   1.00-2.00   sec  1.12 MBytes  9.40 Mbits/sec                  
[  5]   2.00-3.00   sec  1.12 MBytes  9.42 Mbits/sec                  
[  5]   3.00-4.00   sec  1.12 MBytes  9.42 Mbits/sec                  
[  5]   4.00-5.00   sec  1.12 MBytes  9.42 Mbits/sec                  
[  5]   5.00-6.00   sec  1.12 MBytes  9.42 Mbits/sec                  
[  5]   6.00-7.00   sec  1.12 MBytes  9.41 Mbits/sec                  
[  5]   7.00-8.00   sec  1.12 MBytes  9.42 Mbits/sec                  
[  5]   8.00-9.00   sec  1.12 MBytes  9.42 Mbits/sec                  
[  5]   9.00-10.00  sec  1.12 MBytes  9.42 Mbits/sec                  
[  5]  10.00-10.05  sec  60.8 KBytes  9.39 Mbits/sec                  
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate
[  5]   0.00-10.05  sec  11.2 MBytes  9.38 Mbits/sec                  receiver
-----------------------------------------------------------
Server listening on 7575

```

board2:


```
[  5] local 192.168.10.2 port 57014 connected to 192.168.10.1 port 7575
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  5]   0.00-1.00   sec  1.25 MBytes  10.5 Mbits/sec    0   28.3 KBytes       
[  5]   1.00-2.00   sec  1.12 MBytes  9.38 Mbits/sec    0   28.3 KBytes       
[  5]   2.00-3.00   sec  1.12 MBytes  9.38 Mbits/sec    0   28.3 KBytes       
[  5]   3.00-4.00   sec  1.12 MBytes  9.38 Mbits/sec    0   28.3 KBytes       
[  5]   4.00-5.00   sec  1.12 MBytes  9.38 Mbits/sec    0   28.3 KBytes       
[  5]   5.00-6.00   sec  1.12 MBytes  9.38 Mbits/sec    0   28.3 KBytes       
[  5]   6.00-7.00   sec  1.12 MBytes  9.38 Mbits/sec    0   28.3 KBytes       
[  5]   7.00-8.00   sec  1.24 MBytes  10.4 Mbits/sec    0   42.4 KBytes       
[  5]   8.00-9.00   sec  1.12 MBytes  9.38 Mbits/sec    0   42.4 KBytes       
[  5]   9.00-10.00  sec  1.12 MBytes  9.38 Mbits/sec    0   42.4 KBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-10.00  sec  11.4 MBytes  9.60 Mbits/sec    0             sender
[  5]   0.00-10.05  sec  11.2 MBytes  9.38 Mbits/sec                  receiver
```
